CREATE TABLE IF NOT EXISTS `users` (
	`user_id` 		 INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`username` 		 VARCHAR(30) NOT NULL,
	`email` 			 VARCHAR(50) NOT NULL,
	`password`		 VARCHAR(255) NOT NULL,
	`activation_token` VARCHAR(255) NOT NULL,
	`reset_token` 	 VARCHAR(255) NOT NULL,
	`created_at`  	 DATETIME NOT NULL,
	`updated_at`  	 DATETIME
);

CREATE TABLE IF NOT EXISTS `roles` (
	`role_id` 	INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` 	    VARCHAR(30) NOT NULL,
	`description` VARCHAR(255) NOT NULL,
	`created_at`  DATETIME NOT NULL,
	`updated_at`  DATETIME
);

CREATE TABLE IF NOT EXISTS `users_roles` (
	`user_role_id` 	INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`role_id` 		INT(11) UNSIGNED NOT NULL,
	`user_id` 		INT(11) UNSIGNED NOT NULL ,
	`created_at`  	DATETIME NOT NULL,
	`updated_at`  	DATETIME
);

CREATE TABLE IF NOT EXISTS `groups` (
	`group_id` 	INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` 	    VARCHAR(30) NOT NULL,
	`description` VARCHAR(255) NOT NULL,
	`permissions` INT(11) NOT NULL DEFAULT 0,
	`created_at`  DATETIME NOT NULL,
	`updated_at`  DATETIME
);

CREATE TABLE IF NOT EXISTS `users_groups` (
	`user_group_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`group_id` 		INT(11) UNSIGNED NOT NULL,
	`user_id` 		INT(11) UNSIGNED NOT NULL,
	`created_at`  	DATETIME NOT NULL,
	`updated_at`  	DATETIME
);

CREATE TABLE IF NOT EXISTS `bans` (
	`ban_id` 	    INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`user_id` 	INT(11)  NOT NULL,
	`description` VARCHAR(255) NOT NULL,
	`created_at`  DATETIME NOT NULL,
	`ending_at` 	DATETIME,
	`updated_at`  DATETIME
);

CREATE TABLE IF NOT EXISTS `codes` (
	`code_id` 	INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `value`       VARCHAR(10) NOT NULL,
    `codetype_id` INT(11) UNSIGNED NOT NULL,
    `user_id`     INT(11) UNSIGNED,
    `owner_id`    INT(11) UNSIGNED,
    `creator_id`  INT(11) UNSIGNED NOT NULL,
    `used_at`     DATETIME,
    `created_at`  DATETIME NOT NULL,
    `updated_at`  DATETIME
);

CREATE TABLE IF NOT EXISTS `codetypes` (
	`codetype_id` 	INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name`            VARCHAR(30) NOT NULL,
    `created_at`      DATETIME NOT NULL,
    `updated_at`      DATETIME
);
