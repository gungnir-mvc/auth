<?php
namespace Gungnir\Auth\Service;

use \Gungnir\Core\Config;
use \Gungnir\Auth\Repository\UserRepository;
use \Gungnir\Auth\Model\User;
use \Gungnir\Session\Session;


/**
 * Service that should handle everything that is about handling user authentication,
 * creation and other such things.
 */
class AuthenticationService
{
    /** @var UserRepository */
    private $userRepository = null;

    /** @var Session */
    private $session = null;

    /** @var Config [description] */
    private $config = null;

    /**
     * Constructor for AuthenticationService
     *
     * @param UserRepository $userRepository Repository that the service uses to handle userdata
     */
    public function __construct(UserRepository $userRepository, Session $session, Config $config)
    {
        $this->userRepository = $userRepository;
        $this->session = $session;
        $this->config = $config;
    }

    /**
     * Authenticates user by checking given password. Also takes
     * bans and activation state into consideration.
     *
     * @param  User   $user
     * @param  String $password
     *
     * @return bool    True if authentication is successful
     */
    public function authenticateUser(User $user, String $password)
    {
        return $this->authenticateUserPassword($user, $password) &&
               $user->isActivated() &&
               !$user->isBanned();
    }

    /**
     * Validates a registered users password to another passed password
     *
     * @param  User   $user     The user
     * @param  string $password The passed password
     *
     * @return bool             True if password is correct
     */
    public function authenticateUserPassword(User $user, string $password) : bool
    {
        return password_verify($password, $user->getPassword());
    }

    /**
     * Compares two raw passwords strings to ensure that a user
     * that registers have typed correctly.
     *
     * @param  string $firstPassword
     * @param  string $secondPassword
     *
     * @return bool                   True if strings are the same
     */
    public function comparePasswords(string $firstPassword, string $secondPassword) : bool
    {
        return strcmp($firstPassword, $secondPassword) === 0 ? true : false;
    }

    /**
     * Updates a users password to the given one
     *
     * @param  User   $user     The user to set password on
     * @param  String $password Raw new user password
     *
     * @return mixed
     */
    public function updateUserPassword(User $user,String $password)
    {
        return $this->getUserRepository()->updateUserPassword($user, $password);
    }

    /**
     * Validates two passwords against each other
     *
     * @param  String  $password
     * @param  String  $passwordConfirm
     *
     * @return boolean True if password is valid
     */
    public function isPasswordValid(String $password, String $passwordConfirm)
    {
        return strcmp($password, $passwordConfirm) === 0;
    }

    /**
     * Wrapper method for handling registration form data
     * which can require registration codes amongst other things
     * based on configuration.
     *
     * @param  array  $userdata
     *
     * @return bool
     */
    public function registerNewUser(array $userdata)
    {
        $registrationCodeRequired = $this->config->get('registration_code_required');

        if ($registrationCodeRequired) {
            return false;
        }

        return $this->createNewUser($userdata);
    }

    /**
     * Creates a new user based on an array of input data
     *
     * @param  array  $userdata
     *
     * @return bool
     */
    public function createNewUser(array $userdata)
    {
        $user = new User;
        $user->setUsername($userdata['username']);
        $user->setEmail($userdata['email']);
        $user->setPassword($userdata['password']);

        return $this->getUserRepository()->addNewUser($user);
    }

    /**
     * Sets the user as logged in
     *
     * @param User $user
     *
     * @return self
     */
    public function setAsLoggedIn(User $user)
    {
        $this->getSession()->store('__USER', $user);
        return $this;
    }

    /**
     * Logs the user out
     *
     * @return void
     */
    public function logoutUser()
    {
        $this->getSession()->remove('__USER');
    }

    /**
     * Alias for logoutUser()
     *
     * @return void
     */
    public function logout()
    {
        $this->logoutUser();
    }

    /**
     * Determines if a user is logged in or not
     *
     * @return boolean True if user is logged in
     */
    public function isLoggedIn()
    {
        return !empty($this->getSession()->get('__USER'));
    }

    /**
     * Determines if a users account is activated or not
     *
     * @return boolean True if account is activated
     */
    public function isActivated()
    {
        if ($this->isLoggedIn()) {
            return $this->getSession()->get('__USER')->isActivated();
        }
        return false;
    }

    /**
     * Get's currently logged in user
     *
     * @return User|Null
     */
    public function getUser()
    {
        if ($this->isLoggedIn()) {
            return $this->getSession()->get('__USER');
        }

        return null;
    }

    /**
     * Get the value of Service that should handle everything that is about handling user authentication,
     *
     * @return mixed
     */
    public function getUserRepository()
    {
        return $this->userRepository;
    }

    /**
     * Set the value of Service that should handle everything that is about handling user authentication,
     *
     * @param mixed userRepository
     *
     * @return self
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;

        return $this;
    }


    /**
     * Get the value of Session
     *
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set the value of Session
     *
     * @param mixed session
     *
     * @return self
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

}
