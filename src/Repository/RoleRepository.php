<?php
namespace Gungnir\Auth\Repository;

use \Gungnir\Auth\Model\{Role, User};

class RoleRepository extends AbstractRepository
{
    public function getAllRoles()
    {
        return $this->getSource()
                ->select('*', Role::TABLE)
                ->fetchClass(Role::class)
                ->fetchAll();
    }

    public function getUserRoles(User $user)
    {
        return $this->getSource()
                ->select('*', Role::TABLE)
                ->join('users_roles AS ur ON roles.role_id = ur.role_id')
                ->where('ur.user_id', $user->getUserId())
                ->fetchClass(Role::class)
                ->fetchAll();
    }
}
