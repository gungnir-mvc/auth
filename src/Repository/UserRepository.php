<?php
namespace Gungnir\Auth\Repository;

use \Gungnir\Auth\Model\{User, Ban, Role, Group};

class UserRepository extends AbstractRepository
{

    /** @var RoleRepository */
    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Get a user by a given user id
     *
     * @param  Int    $id
     *
     * @return User
     */
    public function getById(Int $id)
    {
        $result = $this->getSource()
                    ->select('*')
                    ->from(User::TABLE)
                    ->where('user_id', $id)
                    ->fetchClass(User::class)
                    ->fetch();
        return $result;
    }

    /**
     * Creates a new user based on an array of input data
     *
     * @param  array  $userdata
     *
     * @return bool
     */
    public function createNewUser(array $userdata)
    {
        $user = new User;
        $user->setUsername($userdata['username']);
        $user->setEmail($userdata['email']);
        $user->setPassword($userdata['password']);
        return $this->addNewUser($user);
    }

    /**
     * Adds a new user to the database
     *
     * @param User $user
     */
    public function addNewUser(User $user)
    {
        if (false === $user->validate()) {
            return false;
        }
        $date = new \DateTime();
        $date = $date->format('c');
        $result = $this->getSource()->insert()->into(User::TABLE)
                    ->columns([
                        'username',
                        'email',
                        'password',
                        'activation_token',
                        'created_at'
                        ])
                    ->values([
                        $user->getUsername(),
                        $user->getEmail(),
                        password_hash($user->getPassword(), PASSWORD_BCRYPT),
                        base64_encode($user->getUsername() . $user->getEmail() . '|' . $date),
                        $date
                        ])
                    ->run();
        return $result;
    }

    public function updateUserPassword(User $user, String $password)
    {
        $date = new \DateTime();
        $date = $date->format('c');
        return $this->getSource()
            ->update('users')
            ->set('password', password_hash($password, PASSWORD_BCRYPT))
            ->set('updated_at', $date)
            ->where('user_id', $user->getUserId())
            ->run();
    }

    /**
     * Retrieves a user from the database based on a string
     * that is predicted to be an email or assumed to be username.
     *
     * @param  String $string Username or Email of wanted user
     *
     * @return User
     */
    public function getUserByString(String $string)
    {
        $query = $this->getSource()->select('*', User::TABLE);

        if (strpos($string, '@') !== false) {
            $query->where('email', $string);
        } else {
            $query->where('username', $string);
        }

        $user = $query->fetchClass(User::class)->fetch();

        if ($user) {
            $user->setRoles($this->roleRepository->getUserRoles($user));
        }

        return $user;
    }

    /**
     * Get all registered bans for a given user
     *
     * @param  User   $user [description]
     *
     * @return Ban[]
     */
    public function getBansForUser(User $user)
    {
        return $this->getSource()->select('*', Ban::TABLE)
                    ->where('user_id', $user->getUserId())
                    ->fetchClass(Ban::class)
                    ->fetchAll();
    }

    /**
     * Get all roles related to a given user
     *
     * @param  User   $user
     *
     * @return Role[]
     */
    public function getRolesForUser(User $user)
    {
        return $this->roleRepository->getUserRoles($user);
    }

    /**
     * Get all groups related to a given user
     *
     * @param  User   $user
     *
     * @return Group[]
     */
    public function getGroupsForUser(User $user)
    {
        return $this->getSource()
                        ->select('*', Group::TABLE)
                        ->join('users_groups AS ug ON users.user_id = ug.user_id')
                        ->where('users.user_id', $user->getUserId())
                        ->fetchAll();
    }

}
