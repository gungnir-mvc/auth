<?php
namespace Gungnir\Auth\Repository;

use \Gungnir\Database\Database;
use \Gungnir\Session\Session;

abstract class AbstractRepository
{
    /** @var Session */
    private $session = null;

    /** @var Database */
    private $source = null;

    /**
     * Get the value of Session
     *
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set the value of Session
     *
     * @param mixed session
     *
     * @return self
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get the value of Source
     *
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set the value of Source
     *
     * @param mixed source
     *
     * @return self
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

}
