<?php
namespace Gungnir\Auth\Controller;

use \Gungnir\Core\{Controller, View};
use \Gungnir\HTTP\{Request, Response};
use \Gungnir\Auth\Controller\Basic\AuthController;

class Index extends AuthController
{

	public function getIndex()
	{
		return $this->getLogin();
	}

	public function postIndex(Request $request)
    {
        return $this->postLogin($request);
    }

	public function getLogin()
	{
        $fallbackPath = VENROOT . 'gungnir-mvc/auth/view/';
		$layout = new View('auth/Layout', $fallbackPath);
		$layout->body = new View('auth/Login', $fallbackPath);

		if ($user = $this->getAuthService()->getUser())
		{
			$layout->body->user = $user;
		}

		return new Response($layout);
	}

	public function postLogin(Request $request)
	{
		$userdata = $request->request();

		$user = $this->getUserRepository()->getUserByString($userdata->get('username'));

		if ($user && $this->getAuthService()->authenticateUser($user, $userdata->get('password'))) {
			$this->getAuthService()->setAsLoggedIn($user);
		}

		return $this->getLogin($request);
	}

	public function getLogout()
	{
		if ($this->getAuthService()->isLoggedIn()) {
			$this->getAuthService()->logout();
		}

		return $this->getLogin();
	}

	public function getRegister()
	{
        $fallbackPath = VENROOT . 'gungnir-mvc/auth/view/';
		$layout = new View('auth/Layout', $fallbackPath);
		$layout->body = new View('auth/Register', $fallbackPath);
		return new Response($layout);
	}

	public function postRegister(Request $request)
	{
		$userdata = $request->request()->parameters();

		if (strcmp($userdata['password'], $userdata['password_confirm']) === 0) {
			$this->getAuthService()->registerNewUser($userdata);
		}

        $fallbackPath = VENROOT . 'gungnir-mvc/auth/view/';
		$layout = new View('auth/Layout', $fallbackPath);
		$layout->body = new View('auth/Register', $fallbackPath);

		return new Response($layout);
	}
}
