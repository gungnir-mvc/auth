<?php
namespace Gungnir\Auth\Controller\Basic;

use \Gungnir\Core\{Controller, Config};
use \Gungnir\Auth\Repository\{UserRepository, RoleRepository};
use \Gungnir\Auth\Service\AuthenticationService;
use \Gungnir\Database\Database;
use \Gungnir\Session\{Session, NativeSessionStorage};
use \Gungnir\Session\Bag\AttributeBag;

class AuthController extends Controller
{
    /** @var UserRepository */
    private $userRepository = null;

    /** @var RoleRepository */
    private $roleRepository = null;

    /** @var AuthenticationService */
    private $authService = null;

    /** @var Config */
    private $authConfig = null;

    public function before()
    {
        parent::before();

        // Set up config that is intended for auth library
        $config = new Config;
        $path = 'config/auth/Settings.php';
        $fallbackPath = VENROOT . 'gungnir-mvc/auth/' . $path;
        $config->loadFromFileCascading($path, $fallbackPath);
        $this->authConfig = $config;

        $db = Database::factory($config->get('database'));
        $session = new Session(new NativeSessionStorage(new AttributeBag));

        $roleRepository = new RoleRepository;
        $roleRepository->setSource($db);

        $userRepository = new UserRepository($roleRepository);

        $userRepository->setSession($session);
        $userRepository->setSource($db);

        $this->userRepository = $userRepository;

        $this->setAuthService(new AuthenticationService($userRepository, $session, $this->authConfig));
    }

    /**
     * Get the value of User Repository
     *
     * @return UserRepository
     */
    public function getUserRepository()
    {
        return $this->userRepository;
    }

    /**
     * Set the value of User Repository
     *
     * @param UserRepository $userRepository
     *
     * @return self
     */
    public function setUserRepository($UserRepository)
    {
        $this->userRepository = $UserRepository;

        return $this;
    }

    /**
     * Get the value of Auth Config
     *
     * @return mixed
     */
    public function getAuthConfig()
    {
        return $this->authConfig;
    }

    /**
     * Set the value of Auth Config
     *
     * @param mixed authConfig
     *
     * @return self
     */
    public function setAuthConfig($authConfig)
    {
        $this->authConfig = $authConfig;

        return $this;
    }


    /**
     * Get the value of Auth Service
     *
     * @return AuthenticationService
     */
    public function getAuthService() : AuthenticationService
    {
        return $this->authService;
    }

    /**
     * Set the value of Auth Service
     *
     * @param AuthenticationService authService
     *
     * @return self
     */
    public function setAuthService(AuthenticationService $authService)
    {
        $this->authService = $authService;

        return $this;
    }

}
