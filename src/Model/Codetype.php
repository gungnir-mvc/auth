<?php
namespace Gungnir\Auth\Model;

class Codetype extends Model
{
    /** @var int */
    private $codetype_id;

    /** @var string */
    private $name;

    /** @var string */
    private $created_at;

    /** @var string */
    private $updated_at;


    /**
     * Get the value of Codetype Id
     *
     * @return mixed
     */
    public function getCodetypeId()
    {
        return $this->codetype_id;
    }

    /**
     * Set the value of Codetype Id
     * 
     * @param mixed codetype_id
     *
     * @return self
     */
    public function setCodetypeId($codetype_id)
    {
        $this->codetype_id = $codetype_id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param mixed name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Created At
     *
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of Created At
     *
     * @param mixed created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of Updated At
     *
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of Updated At
     *
     * @param mixed updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

}
