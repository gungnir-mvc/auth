<?php
namespace Gungnir\Auth\Model;

use \Gungnir\Core\Model;

class User extends Model
{
    const TABLE = 'users';

    protected $required = [
        'username',
        'email',
        'password'
    ];

    private $user_id;
    private $username;
    private $password;
    private $email;
    private $activation_token;
    private $reset_token;
    private $created_at;
    private $updated_at;

    /** @var Group[] Any groups the user belongs to */
    private $groups = [];

    /** @var Ban[] Any bans related to the user */
    private $bans = [];

    /** @var Role[] Any roles associated with the user */
    private $roles = [];

    public function isActivated()
    {
        return empty($this->getActivationToken());
    }

    public function isBanned()
    {
        foreach ($this->bans as $ban) {
            if (time() < strtotime($ban->getEndingAt())) {
                return true;
            }
        }
        return false;
    }

    public function hasRole(String $name)
    {
        foreach ($this->getRoles() as $role) {
            if (strtolower($role->getName()) === strtolower($name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the value of User Id
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set the value of User Id
     *
     * @param mixed user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get the value of Username
     *
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of Username
     *
     * @param mixed username
     *
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of Password
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of Password
     *
     * @param mixed password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of Email
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of Email
     *
     * @param mixed email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of Activation Token
     *
     * @return mixed
     */
    public function getActivationToken()
    {
        return $this->activation_token;
    }

    /**
     * Set the value of Activation Token
     *
     * @param mixed activation_token
     *
     * @return self
     */
    public function setActivationToken($activation_token)
    {
        $this->activation_token = $activation_token;

        return $this;
    }

    /**
     * Get the value of Reset Token
     *
     * @return mixed
     */
    public function getResetToken()
    {
        return $this->reset_token;
    }

    /**
     * Set the value of Reset Token
     *
     * @param mixed reset_token
     *
     * @return self
     */
    public function setResetToken($reset_token)
    {
        $this->reset_token = $reset_token;

        return $this;
    }

    /**
     * Get the value of Created At
     *
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of Created At
     *
     * @param mixed created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of Updated At
     *
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of Updated At
     *
     * @param mixed updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }


    /**
     * Get the value of Groups
     *
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set the value of Groups
     *
     * @param mixed groups
     *
     * @return self
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;

        return $this;
    }

    /**
     * Get the value of Bans
     *
     * @return mixed
     */
    public function getBans()
    {
        return $this->bans;
    }

    /**
     * Set the value of Bans
     *
     * @param mixed bans
     *
     * @return self
     */
    public function setBans($bans)
    {
        $this->bans = $bans;

        return $this;
    }

    /**
     * Get the value of Roles
     *
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set the value of Roles
     *
     * @param mixed roles
     *
     * @return self
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

}
