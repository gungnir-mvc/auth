<?php
namespace Gungnir\Auth\Model;

class Code extends Model
{
    /** @var int */
    private $code_id;

    /** @var string */
    private $value;

    /** @var int */
    private $codetype_id;

    /** @var int */
    private $user_id;

    /** @var int */
    private $owner_id;

    /** @var int */
    private $creator_id;

    /** @var string */
    private $used_at;

    /** @var string */
    private $created_at;

    /** @var string */
    private $updated_at;

    /**
     * Get the value of Code Id
     *
     * @return mixed
     */
    public function getCodeId()
    {
        return $this->code_id;
    }

    /**
     * Set the value of Code Id
     *
     * @param mixed code_id
     *
     * @return self
     */
    public function setCodeId($code_id)
    {
        $this->code_id = $code_id;

        return $this;
    }

    /**
     * Get the value of Value
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set the value of Value
     *
     * @param mixed value
     *
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get the value of Codetype Id
     *
     * @return mixed
     */
    public function getCodetypeId()
    {
        return $this->codetype_id;
    }

    /**
     * Set the value of Codetype Id
     *
     * @param mixed codetype_id
     *
     * @return self
     */
    public function setCodetypeId($codetype_id)
    {
        $this->codetype_id = $codetype_id;

        return $this;
    }

    /**
     * Get the value of User Id
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set the value of User Id
     *
     * @param mixed user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get the value of Owner Id
     *
     * @return mixed
     */
    public function getOwnerId()
    {
        return $this->owner_id;
    }

    /**
     * Set the value of Owner Id
     *
     * @param mixed owner_id
     *
     * @return self
     */
    public function setOwnerId($owner_id)
    {
        $this->owner_id = $owner_id;

        return $this;
    }

    /**
     * Get the value of Creator Id
     *
     * @return mixed
     */
    public function getCreatorId()
    {
        return $this->creator_id;
    }

    /**
     * Set the value of Creator Id
     *
     * @param mixed creator_id
     *
     * @return self
     */
    public function setCreatorId($creator_id)
    {
        $this->creator_id = $creator_id;

        return $this;
    }

    /**
     * Get the value of Used At
     *
     * @return mixed
     */
    public function getUsedAt()
    {
        return $this->used_at;
    }

    /**
     * Set the value of Used At
     *
     * @param mixed used_at
     *
     * @return self
     */
    public function setUsedAt($used_at)
    {
        $this->used_at = $used_at;

        return $this;
    }

    /**
     * Get the value of Created At
     *
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of Created At
     *
     * @param mixed created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of Updated At
     *
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of Updated At
     *
     * @param mixed updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

}
