<?php
namespace Gungnir\Auth\Model;

class Ban
{
    const TABLE = 'bans';

    protected $required = [
        'name',
        'description',
        'permissions'
    ];

    private $ban_id;
    private $user_id;
    private $description;
    private $created_at;
    private $ending_at;
    private $updated_at;

    /**
     * Get the value of Ban Id
     *
     * @return mixed
     */
    public function getBanId()
    {
        return $this->ban_id;
    }

    /**
     * Set the value of Ban Id
     *
     * @param mixed ban_id
     *
     * @return self
     */
    public function setBanId($ban_id)
    {
        $this->ban_id = $ban_id;

        return $this;
    }

    /**
     * Get the value of User Id
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set the value of User Id
     *
     * @param mixed user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param mixed description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Created At
     *
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of Created At
     *
     * @param mixed created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of Ending At
     *
     * @return mixed
     */
    public function getEndingAt()
    {
        return $this->ending_at;
    }

    /**
     * Set the value of Ending At
     *
     * @param mixed ending_at
     *
     * @return self
     */
    public function setEndingAt($ending_at)
    {
        $this->ending_at = $ending_at;

        return $this;
    }

    /**
     * Get the value of Updated At
     *
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of Updated At
     *
     * @param mixed updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

}
