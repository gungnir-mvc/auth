<?php

use Gungnir\HTTP\Route;

Route::add('auth', new Route('/auth/:action/:param', [
		'namespace' => '\Gungnir\Auth\Controller\\',
		'controller' => 'index',
		'defaults' => [
			'action' => 'index',
			'param' => false
		],
	]
));
