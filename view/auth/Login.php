<div class="authform">
    <?php if (empty($user)): ?>
        <form method="post" action="login">
            <label><small>Username</small>
                <input type="text" name="username">
            </label><br>
            <label><small>Password</small>
                <input type="password" name="password">
            </label><br>
            <input type="submit" name="login" value="Login now">
        </form>
        <?php else: ?>
            <form method="get" action="auth/logout">
                <input type="submit" name="logout" value="Logga ut">
            </form>
    <?php endif; ?>
</div>
