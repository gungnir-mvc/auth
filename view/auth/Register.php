<div class="authform">
    <form method="post">
        <label><small>Username</small>
            <input type="text" name="username">
        </label><br>
        <label><small>Email</small>
            <input type="email" name="email">
        </label><br>
        <label><small>Password</small>
            <input type="password" name="password">
        </label><br>
        <label><small>Password confirmation</small>
            <input type="password" name="password_confirm">
        </label><br>
        <input type="submit" name="register" value="Register now">
    </form>
</div>
